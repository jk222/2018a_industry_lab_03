package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        double enteredValue = enteredAmount();
        int enteredDP = enteredDPAmount();
        double truncatedValue = calculateTruncatedValue(enteredValue, enteredDP);
        printTruncatedAmount(truncatedValue, enteredDP);

    }

    private double enteredAmount(){

        System.out.print("Enter a number that includes decimal placing values (e.g 10.263): ");
        String usersEnteredNumber = Keyboard.readInput();
        double enteredValueNumber = Double.parseDouble(usersEnteredNumber);
        return enteredValueNumber;

    }

    private int enteredDPAmount(){

        System.out.print("Enter a number of decimal places: ");
        String usersEnteredDP = Keyboard.readInput();
        int enteredDPNumber = Integer.parseInt(usersEnteredDP);
        return enteredDPNumber;
    }



    private double calculateTruncatedValue(double enteredValue, int enteredDP){

        double calculatedValue1 = (int)(enteredValue * Math.pow(10, enteredDP));
        double calculatedValue2 = calculatedValue1/(Math.pow(10, enteredDP));
        return calculatedValue2;

    }

    private void printTruncatedAmount(double truncatedValue, int enteredDP){

        System.out.println("The amount truncated to " + enteredDP + " decimal places is " + truncatedValue);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}

package ictgradschool.industry.lab03.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    public String getRow(String letters, int row) {

        String textString = "";

        if (row ==1){
            textString = letters.substring(0,6);
        }
        else if (row ==2){
            textString = letters.substring(6,12);
        }
        else if(row ==3){
            textString = letters.substring(12,18);
        }
        return textString;

    }

    public void printRows(String row1, String row2, String row3) {

        System.out.println(row1);
        System.out.println(row2);
        System.out.println(row3);
    }

    public String getLeftDiagonal(String row1, String row2, String row3) {

        String LeftDiagonalString = row1.charAt(0) + " " + row2.charAt(2) + " " + row3.charAt(4);
        return LeftDiagonalString;
    }

    public void printDiagonal(String leftDiagonal) {

        System.out.println("Diagonal: " + leftDiagonal);

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}

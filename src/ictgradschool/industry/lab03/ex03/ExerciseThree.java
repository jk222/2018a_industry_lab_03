package ictgradschool.industry.lab03.ex03;

import ictgradschool.Keyboard;
import sun.awt.windows.ThemeReader;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class ExerciseThree {

    private void start() {

        String sentence = getSentenceFromUser();

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return
     */
    private String getSentenceFromUser() {

        System.out.print("Enter a sentence: ");
        String usersEnteredSentence = Keyboard.readInput();
        return usersEnteredSentence;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        int sentenceLength = sentence.length();
        int randomPositionNumber = (int)(Math.random()*sentenceLength);
        return randomPositionNumber;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {


        char characterRemoved = sentence.charAt(position);
        System.out.println("The character removed is: '" + characterRemoved + "' from position " + position);
    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence, int position) {

        String alteredSentence1 = sentence.substring(0, position);
        String alteredSentence2 = sentence.substring(position+1, sentence.length());

        String alteredSentence3 = alteredSentence1 + alteredSentence2;

        return alteredSentence3;
    }

    /**
     * Prints a message which shows the new sentence after the removal has occurred.
     */
    private void printNewSentence(String changedSentence) {

        System.out.println("The new sentence is '" + changedSentence +"'");
    }

    public static void main(String[] args) {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}

package ictgradschool.industry.lab03.ex02;

import com.sun.org.apache.xml.internal.security.Init;
import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {




    
    private void start() {

        System.out.println("Enter a number for a lower bound value of two numbers");

        String lowerBoundValue = Keyboard.readInput();
        int lowerValue = Integer.parseInt(lowerBoundValue);

        System.out.println("Enter a number for an upper bound value of two numbers");
        String upperBoundValue = Keyboard.readInput();
        int upperValue = Integer.parseInt(upperBoundValue);

        int randomValue1 = randomValueGenerator(lowerValue, upperValue);
        int randomValue2 = randomValueGenerator(lowerValue, upperValue);
        int randomValue3 = randomValueGenerator(lowerValue, upperValue);
        System.out.println("3 randomly generated numbers are: " + randomValue1 + ", " + randomValue2 + ", " + randomValue3);

        int Value1 = smallestValueCalculator(randomValue1, randomValue2);
        int Value2 = smallestValueCalculator(randomValue2, randomValue3);
        int Value3 = smallestValueCalculator(Value1, Value2);
        System.out.println("The smallest number is " + Value3);



    }

    private int randomValueGenerator(int lowerBound, int upperBound){

        int randomValue = (int)(Math.random() *(upperBound - lowerBound +1)) + lowerBound;

        return randomValue;
    }

    private int smallestValueCalculator(int smallValue1, int smallValue2){

        int smallValue = Math.min(smallValue1, smallValue2);

        return smallValue;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
